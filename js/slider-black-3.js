var mySpreadsheetD = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=1763534191";

// Compile the Handlebars template for HR leaders.
var HRTemplateD = Handlebars.compile($('#newSlider-guest-2').html());

// Load top five HR leaders.
$('#newSlider-guest-2').sheetrock({
    url: mySpreadsheetD,
    query: "select A,B,C,D,E,F",
    labels: ["ID", "MOB-ID", "TITLE", "DESC", "NEW", "IMG"],
    fetchSize: 0,
    rowTemplate: HRTemplateD,
    callback: function(error){
        if (error === null){
            $("#newSlider-guest-2").slick({
                dots: true,
                arrows: true,
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 780,
                        settings: {
                            dots: true,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });
        }
    },
});
//$( ".prodSlideBot-01").attr("src", "{{cells.IMG}}").parent().parent().parent().parent().remove();