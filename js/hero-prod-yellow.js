var imageWidth = 1920,
    imageHeight = 1332,
    imageAspectRatio = imageWidth / imageHeight,
    $window = $(window);

var hotSpots = [{
      'x': -310,
      'y': -75
    }, {
      'x': 100,
      'y': -30
    }, {
      'x': 790,
      'y': 100
    }, {
      'x': 210,
      'y': 220
    }, {
      'x': -310,
      'y': 240
    }];

var mySpreadsheet = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=1066397729";

// Compile the Handlebars template for HR leaders.
var HRTemplate = Handlebars.compile($('#hero-prod').html());

// Load top five HR leaders.
$('#hero-prod').sheetrock({
    url: mySpreadsheet,
    query: "select A,B,C,D,E,F,G",
    labels: ["ID", "MOB-ID", "TITLE", "DESC", "NEW", "IMG", "BOX"],
    fetchSize: 0,
    rowTemplate: HRTemplate,
});
//$( ".prodSlideBot").attr("src", "{{cells.IMG}}").parent().parent().remove();