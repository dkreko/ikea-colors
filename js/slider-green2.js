var mySpreadsheetC = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=1305093260";

// Compile the Handlebars template for HR leaders.
var HRTemplateC = Handlebars.compile($('#newSlider-guest-1').html());

// Load top five HR leaders.
$('#newSlider-guest-1').sheetrock({
    url: mySpreadsheetC,
    query: "select A,B,C,D,E,F",
    labels: ["ID", "MOB", "TITLE", "DESC", "NEW", "IMG"],
    fetchSize: 0,
    rowTemplate: HRTemplateC,
    callback: function(error){
        if (error === null){
            $("#newSlider-guest-1").slick({
                dots: true,
                arrows: true,
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 780,
                        settings: {
                            dots: true,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });
        }
    },
});
//$( ".prodSlideBot-01").attr("src", "{{cells.IMG}}").parent().parent().parent().parent().remove();