var imageWidth = 1920,
    imageHeight = 1206,
    imageAspectRatio = imageWidth / imageHeight,
    $window = $(window);

var hotSpots = [{
      'x': -380,
      'y': 50
    }, {
      'x': -780,
      'y': 40
    }, {
      'x': 260,
      'y': 30
    }, {
      'x': -110,
      'y': 170
    }, {
      'x': 520,
      'y': -100
    }, {
      'x': 250,
      'y': -180
    }];

var mySpreadsheet = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=1295186064";

// Compile the Handlebars template for HR leaders.
var HRTemplate = Handlebars.compile($('#hero-prod').html());

// Load top five HR leaders.
$('#hero-prod').sheetrock({
    url: mySpreadsheet,
    query: "select A,B,C,D,E,F,G",
    labels: ["ID", "MOB-ID", "TITLE", "DESC", "NEW", "IMG", "BOX"],
    fetchSize: 0,
    rowTemplate: HRTemplate,
});
//$( ".prodSlideBot").attr("src", "{{cells.IMG}}").parent().parent().remove();