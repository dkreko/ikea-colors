var mySpreadsheetB = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=1233761411";

// Compile the Handlebars template for HR leaders.
var HRTemplateB = Handlebars.compile($('#newSlider-guest').html());

// Load top five HR leaders.
$('#newSlider-guest').sheetrock({
    url: mySpreadsheetB,
    query: "select A,B,C,D,E,F",
    labels: ["ID", "MOB", "TITLE", "DESC", "NEW", "IMG"],
    fetchSize: 0,
    rowTemplate: HRTemplateB,
    callback: function(error){
        if (error === null){
            $("#newSlider-guest").slick({
                dots: true,
                arrows: true,
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 780,
                        settings: {
                            dots: true,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });
        }
    },
});
$( ".prodSlideBot-01").attr("src", "{{cells.IMG}}").parent().parent().parent().parent().remove();