function appendHotSpots() {
      for (var i = 0; i < hotSpots.length; i++) {
        var hotSpotId = i + 1;
        var radnomDelay = Math.random() * hotSpotId;
        var $hotSpot = $('<div id="0' + hotSpotId + '">').addClass('hot-spot drop0' + hotSpotId)
        $('.hero-container').append($hotSpot);
      }
      positionHotSpots();
    }

function appendSpeechBubble() {
  var $speechBubble = $('<div>').addClass('speech-bubble');
  $('.hero-container').append($speechBubble);
}

function handleHotSpotMouseover(e) {
  var $currentHotSpot = $(e.currentTarget),
      currentIndex = $currentHotSpot.index(),
      // $speechBubble = $('.speech-bubble'),
      // title = hotSpots[currentIndex]['title'],
      // description = hotSpots[currentIndex]['description'],
      hotSpotTop = $currentHotSpot.offset().top,
      hotSpotLeft = $currentHotSpot.offset().left,
      hotSpotHalfSize = $currentHotSpot.width() / 2,
      speechBubbleHalfSize = $speechBubble.width() / 2,
      topTarget = hotSpotTop - $speechBubble.height(),
      leftTarget = (hotSpotLeft - (speechBubbleHalfSize)) + hotSpotHalfSize;

  $speechBubble.empty();
  $speechBubble.append($('<h1>').text(title));
  $speechBubble.append($('<p>').text(description));

  $speechBubble.css({
    'top': topTarget - 20,
    'left': leftTarget,
    'display': 'block'
  }).stop().animate({
    opacity: 1
  }, 200);
}

function handleHotSpotMouseout(){
  var $speechBubble = $('.speech-bubble');
  $speechBubble.stop().animate({
    opacity: 0
  }, 200, function(){
    $speechBubble.hide();
  });
}

function positionHotSpots() {
  var windowWidth = $window.width(),
    windowHeight = $('.hero-product-01').height(),
//    $('.hero-product-01').height()
    windowAspectRatio = windowWidth / windowHeight,
    $hotSpot = $('.hot-spot');

  $hotSpot.each(function(index) {
    var xPos = hotSpots[index]['x'],
        yPos = hotSpots[index]['y'],
        desiredLeft = 0,
        desiredTop = 0;

    if (windowAspectRatio > imageAspectRatio) {
      yPos = (yPos / imageHeight) * 100;
      xPos = (xPos / imageWidth) * 100;
    } else {
      yPos = ((yPos / (windowAspectRatio / imageAspectRatio)) / imageHeight) * 100;
      xPos = ((xPos / (windowAspectRatio / imageAspectRatio)) / imageWidth) * 100;
    }

    $(this).css({
      'margin-top': yPos + '%',
      'margin-left': xPos + '%'
    });

  });
}

appendHotSpots();
appendSpeechBubble();
$(window).resize(positionHotSpots);
$('.hot-spot').on('mouseover', handleHotSpotMouseover);
$('.hot-spot').on('mouseout', handleHotSpotMouseout);

$('#colorBox1').click(function(){
  $('.hero-container').removeClass('slika2');
  $('.hero-container').removeClass('slika3');
  
});
$('#colorBox2').click(function(){
  $('.hero-container').removeClass('slika3');
  $('.hero-container').addClass('slika2');
  
});
$('#colorBox3').click(function(){
  $('.hero-container').removeClass('slika2');
  $('.hero-container').addClass('slika3');
});

            $('#01').click(function(){
                $('#box-01').fadeToggle();
                $('#box-02').hide();
                $('#box-03').hide();
                $('#box-04').hide();
                $('#box-05').hide();
                $('#box-06').hide();
                $('#box-07').hide();
            });
            $('#02').click(function(){
                $('#box-02').fadeToggle();
                $('#box-01').hide();
                $('#box-03').hide();
                $('#box-04').hide();
                $('#box-05').hide();
                $('#box-06').hide();
                $('#box-07').hide();
            });
            $('#03').click(function(){
                $('#box-03').fadeToggle();
                $('#box-01').hide();
                $('#box-02').hide();
                $('#box-04').hide();
                $('#box-05').hide();
                $('#box-06').hide();
                $('#box-07').hide();
            });
            $('#04').click(function(){
                $('#box-04').fadeToggle();
                $('#box-01').hide();
                $('#box-02').hide();
                $('#box-03').hide();
                $('#box-05').hide();
                $('#box-06').hide();
                $('#box-07').hide();
            })
            $('#05').click(function(){
                $('#box-05').fadeToggle();
                $('#box-01').hide();
                $('#box-02').hide();
                $('#box-03').hide();
                $('#box-04').hide();
                $('#box-06').hide();
                $('#box-07').hide();
            });
            
            $('#06').click(function(){
                $('#box-06').fadeToggle();
                $('#box-01').hide();
                $('#box-02').hide();
                $('#box-03').hide();
                $('#box-04').hide();
                $('#box-05').hide();
                $('#box-07').hide();
            });
            
            $('#07').click(function(){
                $('#box-07').fadeToggle();
                $('#box-01').hide();
                $('#box-02').hide();
                $('#box-03').hide();
                $('#box-04').hide();
                $('#box-05').hide();
                $('#box-06').hide();
            });