var imageWidth = 1920,
    imageHeight = 1293,
    imageAspectRatio = imageWidth / imageHeight,
    $window = $(window);

var hotSpots = [{
      'x': 380,
      'y': 160
    }, {
      'x': 510,
      'y': -105
    }, {
      'x': 20,
      'y': -60
    }, {
      'x': -480,
      'y': 310
    }, {
      'x': -75,
      'y': 210
    }];

var mySpreadsheet = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=1600902208";

// Compile the Handlebars template for HR leaders.
var HRTemplate = Handlebars.compile($('#hero-prod').html());

// Load top five HR leaders.
$('#hero-prod').sheetrock({
    url: mySpreadsheet,
    query: "select A,C,D,E,F,G",
    labels: ["ID", "TITLE", "DESC", "NEW", "IMG", "BOX"],
    fetchSize: 0,
    rowTemplate: HRTemplate,
});
//$( ".prodSlideBot").attr("src", "{{cells.IMG}}").parent().parent().remove();