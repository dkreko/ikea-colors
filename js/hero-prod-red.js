var imageWidth = 1920,
    imageHeight = 1740,
    imageAspectRatio = imageWidth / imageHeight,
    $window = $(window);

var hotSpots = [{
      'x': -50,
      'y': 50
    }, {
      'x': -350,
      'y': -40
    }, {
      'x': -350,
      'y': 80
    }, {
      'x': 400,
      'y': 50
    }, {
      'x': 75,
      'y': 210
    }, {
      'x': 680,
      'y': 140
    }];

var mySpreadsheet = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=2057369970";

// Compile the Handlebars template for HR leaders.
var HRTemplate = Handlebars.compile($('#hero-prod').html());

// Load top five HR leaders.
$('#hero-prod').sheetrock({
    url: mySpreadsheet,
    query: "select A,B,C,D,E,F,G",
    labels: ["ID", "MOB-ID", "TITLE", "DESC", "NEW", "IMG", "BOX"],
    fetchSize: 0,
    rowTemplate: HRTemplate,
});
//$( ".prodSlideBot").attr("src", "{{cells.IMG}}").parent().parent().remove();