var imageWidth = 1920,
    imageHeight = 1206,
    imageAspectRatio = imageWidth / imageHeight,
    $window = $(window);

var hotSpots = [{
      'x': 80,
      'y': 110
    }, {
      'x': 370,
      'y': -130
    }, {
      'x': -190,
      'y': 0
    }, {
      'x': -270,
      'y': 40
    }, {
      'x': 455,
      'y': 65
    }, {
      'x': 380,
      'y': 0
    }, {
      'x': -110,
      'y': -180
    }];

var mySpreadsheet = "https://docs.google.com/spreadsheets/d/1gx7Astq_Cg8LwW_E9bsdXfa6W_y_WM_mdtOeRM_oSls/edit#gid=888546816";

// Compile the Handlebars template for HR leaders.
var HRTemplate = Handlebars.compile($('#hero-prod').html());

// Load top five HR leaders.
$('#hero-prod').sheetrock({
    url: mySpreadsheet,
    query: "select A,C,D,E,F,G",
    labels: ["ID", "TITLE", "DESC", "NEW", "IMG", "BOX"],
    fetchSize: 0,
    rowTemplate: HRTemplate,
});
//$( ".prodSlideBot").attr("src", "{{cells.IMG}}").parent().parent().remove();
